// DOM - document object model

// querySelector function takes a string input that is formatted like CSS selector when applying styles. This allows us to get a specific element
// we can contain the code inside a constant

// alternative
// document.getElementById()
// document.getElementsByClassName()
// document.getElementbyByTagName()


// console.log(document)


const txtFirstName = document.querySelector(`#txt-first-name`)
const spanFullName = document.querySelector(`#span-full-name`)
const txtLastName = document.querySelector(`#txt-last-name`)


const updateFullName = () => {

	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${firstName} ${lastName}`
}


txtFirstName.addEventListener('keyup', updateFullName)
txtLastName.addEventListener('keyup', updateFullName)	

// const fullName = document.querySelectorAll(`#txt-first-name, #txt-last-name`)

// 	// txtFirstName.addEventListener('keyup', (event) => {
// 	// 	spanFullName.innerHTML = txtFirstName.value;
// 	// })

// 	// txtFirstName.addEventListener('keyup', (e) => {
// 	// 	console.log(e.target)
// 	// 	console.log(e.target.value)
// 	// })

// 	fullName[0].addEventListener('keyup', (e) => {
// 		spanFullName.innerHTML = fullName[0].value + ` ` + fullName[1].value
// 	})

// 	fullName[1].addEventListener('keyup', (e) => {
// 		spanFullName.innerHTML = fullName[0].value + ` ` + fullName[1].value
// 	})


// Correct Solution